import axios from 'axios';

export var url="http://localhost:4000";
export function authHeader() {
    const accessToken = localStorage.getItem('accessToken');
  
    if (accessToken) {
      
      return { 'token': accessToken };
    } else {
      return {};
    }
  }
export function checkError(err){

  let refreshToken = localStorage.getItem("refreshToken");
      if (
        refreshToken &&
        err.response.status === 401 &&
        !err.config._retry) {
        err.config._retry = true;
        axios
          .post(url+'/user/refreshToken', { refreshToken: refreshToken })
          .then((res) => {
            if (res.status === 200) {
              localStorage.setItem("accessToken", res.data.accessToken);
              
            }
          });
      }
    
      else
      {
        return false;
        
      }
    
}