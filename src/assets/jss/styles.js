import { makeStyles } from '@material-ui/core/styles';



export const useStyles = makeStyles((theme) => ({
    rootUser: {
      flexGrow:1,
      
    },
    root: {
      
      display:'flex'
    },
    
    
    
    greeting:{
      
      marginLeft:'auto'
    },
    title: {
      flexGrow: 1,
    },
    
    toolbar: theme.mixins.toolbar,
    
    contentUser: {
      flexGrow: 1,
      padding: theme.spacing(2),
      marginLeft:theme.spacing(2),
      marginTop:theme.spacing(8)
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(2),
      marginLeft:theme.spacing(1),
    },
  
    
    table: {
      minWidth: 700,
    },
    visuallyHidden: {
      border: 0,
      clip: 'rect(0 0 0 0)',
      height: 1,
      margin: -1,
      overflow: 'hidden',
      padding: 0,
      position: 'absolute',
      top: 20,
      width: 1,
    },

    paper: {
        width:'100%',
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      },

    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
      },

    form: {
        width: '100%', 
        marginTop: theme.spacing(1),
      },

    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
      },

    selectEmpty: {
        marginTop: theme.spacing(2),
      },
      menuButton: {
        marginRight: theme.spacing(2),
      },
      
    
      
    psColor: {
	color: '#BC2B7F',
      },
    rankColor: { 
	color: '#F05731',
	fontWeight: 'bold',
      },
    surveyTitleColor: {
	color: '#7800DA',
      },
    surveyTableBorder: {
        bordeRightWidth: 1,
	borderColor: '#2EEDB4',
	borderStyle: 'solid',
    },
    surveyTableCellBorder: {
	borderWidth: 1,
	borderColor: '#2EEDB4',
	borderStyle: 'solid',
    },
    linkStyle:{
      color:'inherit',
      textDecoration:'inherit'
    },
    submit: {
      background: 'linear-gradient(45deg, #EB5000 30%, #F03782 90%)',
      borderRadius: 3,
      border: 0,
      color: 'white',
margin: theme.spacing(3, 0, 2),
  },
  deleteButton: {
      "&:hover": {
        backgroundColor: '#000000',
      },
      backgroundColor: '#000000',
      borderRadius: 3,
      border: 0,
      color: 'white',
      margin: theme.spacing(3, 0, 2),
  }
}));
