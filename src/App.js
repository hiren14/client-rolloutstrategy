import React from 'react';
import './App.css';
import Login from './components/login';
import ManageBU from './components/manageBU'
import UserHome from './components/userHome';
import AdminHome from './components/adminHome';
import ProgramOwner from './components/programOwnerHome'
import EditProgramOwnerTemplate from './components/editProgramOwnerTemplate'
import ShowResponses from './components/showResponses'
import SurveyCompletionReport from './components/surveyCompletionReport'
import ManageProgramOwnerUser from './components/manageProgramOwnerUser'
import ManageProgramOwnerBU from './components/manageProgramOwnerBU'
import ManageProducts from './components/manageProducts'
import ShowTemplate from './components/showTemplate';
import AddTemplate from './components/addTemplate';
import ManageUser from './components/manageUser';
import EditTemplate from './components/editTemplate';
import ForgetPassword from './components/forgetPassword';

import { BrowserRouter as Router, Route,Switch } from "react-router-dom";

function App() {
  return (
    <div>
      <br/>
      
      <Router>
        <Switch>
          <Route exact path='/' component={Login} />
          <Route exact path='/forgetPassword' component={ForgetPassword} />
          <Route exact path='/userHome' component={UserHome}/>
          <Route exact path='/adminHome' component={ AdminHome } />
          <Route exact path='/programOwner' component={ProgramOwner}/>
          <Route exact path='/editProgramOwnerTemplate/:id' component={EditProgramOwnerTemplate}/>
          <Route exact path='/showResponses/:id' component={ShowResponses}/>
          <Route exact path='/surveyCompletionReport/:id' component={SurveyCompletionReport}/>
          <Route exact path='/manageProgramOwnerBU/' component={ManageProgramOwnerBU}/>
          <Route exact path='/manageProgramOwnerUser/' component={ManageProgramOwnerUser}/>
          <Route exact path='/manageProducts/' component={ManageProducts}/>
          <Route exact path='/showTemplate/:id' component={ ShowTemplate } />
          <Route exact path='/addTemplate' component={AddTemplate}/>
          
          <Route exact path='/manageBU' component={ManageBU}/>
          <Route exact path='/manageUser' component={ManageUser}/>
          <Route exact path='/editTemplate/:id' component={EditTemplate}/>

        </Switch>
      </Router>
    </div>
  );
}

export default App;
