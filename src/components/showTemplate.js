import React,{useEffect,useState} from 'react'
import SidebarMenu from './sidebarMenu'
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import axios from 'axios';
import {authHeader ,url,checkError} from '../commonFile'
import {useStyles} from '../assets/jss/styles'
import Alert from '@material-ui/lab/Alert';
import { Link } from "react-router-dom";


export default function ShowTemplate(props) {

  const classes = useStyles();
  
  const [errmsg,setErrmsg]=useState("")
  const [template,setTemplate]=useState("")
  const [questions,setQuestions]=useState([])
  const [id,setId]=useState(props.match.params.id)
  
  
  const [didMount, setDidMount] = useState(false); 
  
  useEffect(() => {
    setDidMount(true)
    let token=localStorage.getItem("accessToken")
    
    
  
    
    if(!token)
      props.history.push('/')

    axios.get(url+'/admin/getTemplate',{headers:authHeader(),params:{tid:id}})
    .then(res=>{

      
        
        if(res.data)
        {
          setTemplate(res.data.templateName)
          setId(res.data._id)
          setQuestions(res.data.questions)
        //   setCount(res.data.length)
          
        }
        
    })
    .catch(err=>{

      if(err && err.response.data=="Invalid usertype")
      {
        localStorage.removeItem("accessToken")
        localStorage.removeItem("refreshToken")
        localStorage.removeItem("userdetails")
        props.history.push('/')
      }
      else
      {
        var flag=checkError(err)
        if(flag==false)
        {
        setErrmsg(err.response.data)
        }

      }
      })
    return()=>setDidMount(false)
    
  },[])

  if(!didMount) {
    
    return null;
  }
  const onDelete=(e)=>
  {
    e.preventDefault()
    
    axios.delete(url+'/admin/deleteTemplate',{headers:authHeader(),data:{tid:id}})
    .then(()=>{
      
      props.history.push('/adminHome')
    })
    .catch(err=>checkError(err))
  }


  return (
    <div className={classes.root}>
      <CssBaseline/>
      <SidebarMenu usertype="admin" />
      
        <main className={classes.content}>
          <div className={classes.toolbar} />
          <Typography variant="h4" align="center">
              {template}
          </Typography>
          <br/>
          {errmsg?<Alert severity="error">{errmsg}</Alert>:<div/>}
          <br/>
          
          <div>
            {
              questions.map((item,index)=>{
                return(
                  <div key={index}>
                  <Typography variant="h6">
                     Question {index+1} - &nbsp; { item[0]}
                  </Typography>
                  <ul>
                  {
                    item[2].map((subitem,sindex)=>{
                      return <li key={sindex}><Typography variant="body1">{subitem.desc}</Typography></li>
                    })
                  }
                  </ul>
                    
                  
                  </div>

                )
                
              })
            }
            <Link to={'/editTemplate/'+id} className={classes.linkStyle}>
            <Button
                variant="contained"
                color="secondary"
                className={classes.submit}
                >
                Edit Template
            </Button>
            </Link>
            &nbsp;
            <Button
                variant="contained"
                color="secondary"
                onClick={onDelete}
                className={classes.deleteButton}
            >
                Delete Template
            </Button>
          </div>

        </main>
      
    </div>
  )
}