import React,{useEffect,useState} from 'react'
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import {authHeader ,url,checkError} from '../commonFile'
import {useStyles} from '../assets/jss/styles'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import PowerSettingsNewIcon from '@material-ui/icons/PowerSettingsNew';
import Alert from '@material-ui/lab/Alert';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import RolloutStrategy from '../assets/images/RolloutStrategy.png';

export default function UserHome(props) {

  const classes = useStyles();
  const [errmsg,setErrmsg]=useState("")
  const [errmsg1,setErrmsg1]=useState("")
  const [tid,setTid]=useState("")
  const [questions,setQuestions]=useState([])
  const [products,setProducts]=useState([])
  const [product,setProduct]=useState("")
  const [status,setStatus]=useState("")
  const [values,setValues]=useState([])
  const [check,setCheck]=useState(true)
  const [resStatus,setResStatus]=useState("")
  const [didMount, setDidMount] = useState(false); 
  

  const onChangeValue=(e,ind)=>
  {
    let narr=[...values]
    let level="";
    for(let i=0;i<questions[ind][2].length;i++)
    {
      if(questions[ind][2][i].desc==e.target.value)
        level=parseInt(questions[ind][2][i].level)
    }
    narr[ind]={"question":questions[ind][0],"category":questions[ind][1],"ans":[e.target.value,level]}
    setValues(narr)
    
  }

  const onChangeProduct=(e)=>
  {
    setProduct(e.target.value)
  }

  const onSubmit=(e)=>
  {
    e.preventDefault()
    
    const data=
    {
      "tid":tid,
      "response":values,
      "product":product
    }
    if(product=="")
      setErrmsg1("Please select the product")
    else if(values.includes(""))
     setErrmsg1("Please fill the full survey")
    else{
    axios.post(url+'/user/addResponse',data,{headers:authHeader()})
    .then(res=>
      {
        setResStatus(res.data.message)
      })
      .catch(err=>{
        var flag=checkError(err)
      if(flag==false)
      {
      setErrmsg(err.response.data)
      }
    })
    }
  }

  useEffect(() => {
    setDidMount(true)

    
    let token=localStorage.getItem("accessToken")
    
    
    
    if(!token)
      props.history.push('/')
    axios.get(url+'/user/home',{headers:authHeader()})
    .then(res=>{

        
        
        if(res.data.message=="ok" && res.data.template)
        {
          
          setQuestions(res.data.template.questions)
          setTid(res.data.template._id)
          setProducts(res.data.productData)
          let n=[]
          for(let i=0;i<res.data.template.questions.length;i++)
          {
            
            n.push("")
            
          }
          setValues(n)
          setCheck(false)
          
        }
        else
        {
          setStatus(res.data.message)
        }
    })
    .catch(err=>
      {

      if(err && err.response.data=="Invalid usertype")
      {
        localStorage.removeItem("accessToken")
        localStorage.removeItem("refreshToken")
        localStorage.removeItem("userdetails")
        props.history.push('/')
      }
      else
      {
        var flag=checkError(err)
        if(flag==false)
        {
        setErrmsg(err.response.data)
        }

  }
      }
      )
    
    return()=>setDidMount(false)
    
  },[])

  if(!didMount) {
    
    return null;
  }

  const logout=()=>
  {
    let rt=localStorage.getItem("refreshToken")
    localStorage.removeItem("refreshToken")
    localStorage.removeItem("accessToken")
    localStorage.removeItem("userdetails")
    axios.delete(url+'/user/logout',{data:{refreshToken:rt}})
    .then(()=>{
      props.history.push('/')
    })
    .catch(err=>{
      var flag=checkError(err)
    if(flag==false)
    {
    setErrmsg(err.response.data)
    }
  })
  }
  
  const showSurvey=()=>
  {
    return(
        <div>
          {errmsg?<Alert severity="error">{errmsg}</Alert>:<div/>}
          {
            resStatus==""?
          <form>
          {errmsg1?<Alert severity="error">{errmsg1}</Alert>:<div/>}
          <FormControl className={classes.formControl} style={{minWidth: 140}}>
                <InputLabel id="product">Select Product</InputLabel>
                <Select
                  labelId="product"
                  id="product"
                  value={product}
                  onChange={onChangeProduct}
                >
                  { products.map((item,index)=>{
                    return <MenuItem key={index} value={item.productName}>{item.productName}</MenuItem>
                  }) }
                  
                 
                </Select> 
          </FormControl>
          {
            
            
            questions.map((item,index)=>{
              return(
                <div key={index}>
                <br/>
                <FormControl component="fieldset" >
                  <Typography variant="h6">Question {index+1}: {item[0]}</Typography>
                  <br/>
                  <RadioGroup aria-label={"question"+(index+1)} name={"question"+(index+1)}  onChange={(val)=>onChangeValue(val,index)}  >
                    {
                        item[2].map((subitem,sindex)=>{
                          return <FormControlLabel key={sindex} value={subitem.desc} control={<Radio />} checked={check || values[index].ans && values[index].ans[0] ==subitem.desc} label={subitem.desc} />
                        })

                    }  
                  </RadioGroup>
                </FormControl>
                
                </div>
                

              )
              
            })
          }
          <Button
          type="submit"
          variant="contained"
          color="primary"
          onClick={(e)=>onSubmit(e)}
          className={classes.submit}
          >
          Submit
         </Button>
          </form>
        
        :<div><Typography variant="h6">
        {resStatus?<Alert severity="success">{resStatus}</Alert>:<div/>}
        
    </Typography>
    {products.length>1?<div>
      <br/>
    <Typography variant="h6">
          Do you want to submit response for another product?
    </Typography>
    
    <Button  type="submit"
          variant="contained"
          color="primary"
          onClick={() => window.location.reload()}
          className={classes.submit}>
      Yes
    </Button>
    
    </div>:<div><br/><Typography variant="h6">You have submitted response for all products</Typography>
    </div>
  }
  </div>
      }

      </div>
      
    )
    
  }
  

  return (
    <div className={classes.rootUser}>
      <CssBaseline />
      <AppBar position="fixed" style={{backgroundColor: '#000000',
      color: '#F03782',}}>
        <Toolbar>
        <img src={RolloutStrategy} alt="Rollout Strategy"/>
          <Typography variant="h6" className={classes.title}>
            Rollout Strategy
          </Typography>
          <div>
          <Typography variant="body1" className={classes.title}>
            hello { JSON.parse(localStorage.getItem("userdetails")).username }
          </Typography>
          <Button color="inherit" onClick={logout}><PowerSettingsNewIcon/> Logout</Button>
          </div>
        </Toolbar>
      </AppBar>
      
        <main className={classes.contentUser}>
          {/* <div className={classes.toolbar} /> */}
          <Typography variant="h4" align="center">
              User Home Page
          </Typography>
          <br/>
          {
            status==""?showSurvey():<Typography variant="h6">
            {status}
        </Typography>
          }
        </main>
      
    </div>
  )
}
