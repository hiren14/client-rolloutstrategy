import React,{useEffect,useState} from 'react'
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Alert from '@material-ui/lab/Alert';
import axios from 'axios';
import {authHeader ,url,checkError} from '../commonFile'
import {useStyles} from '../assets/jss/styles'
import PropTypes from 'prop-types';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Paper from '@material-ui/core/Paper';
import SidebarMenu from './sidebarMenu'
import Grid from '@material-ui/core/Grid';
import Chart from "react-google-charts";




function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

const headCells = [
  { id: 'bu', numeric: false, disablePadding: true, label: 'Business Unit' },
  { id: 'pn', numeric: false, disablePadding: false, label: 'Product Name' },
  { id: 'pc', numeric: false, disablePadding: false, label: 'Product Code' },
  { id: 'bv', numeric: true, disablePadding: false, label: 'Business Value Score (A)' },
  { id: 'ic', numeric: true, disablePadding: false, label: 'Implementation Complexity Score (B)' },
  { id: 'ps', numeric: true, disablePadding: false, label: 'Priority Score (A+B)' },
  { id: 'rank', numeric: true, disablePadding: false, label: 'Rank' },
];

function EnhancedTableHead(props) {
  const { classes, onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        
        {headCells.map((headCell) => (
          <TableCell
            className={classes.surveyTableCellBorder}
            key={headCell.id}
            align={headCell.numeric ? 'center' : 'center'}
            padding={headCell.disablePadding ? 'default' : 'default'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              className={headCell.id=='ps'?classes.psColor:headCell.id=='rank'?classes.rankColor:'black'}
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};








export default function ShowResponses(props) {

  const classes = useStyles();
  const [errmsg,setErrmsg]=useState("")
  const [tid]=useState(props.match.params.id)
  const [responses,setResponses]=useState([])
  const [chartData1,setChartdata]=useState([['Implementation Complexity', 'Business Value',{ role: "tooltip", type: "string", p: { html: true } }]])
  
  const [didMount, setDidMount] = useState(false); 
  const [status,setStatus]=useState("")
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('calories');
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [dense, setDense] = React.useState(false);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = responses.map((n) => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleChangeDense = (event) => {
    setDense(event.target.checked);
  };

  const isSelected = (name) => selected.indexOf(name) !== -1;

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, responses.length - page * rowsPerPage);

  
  useEffect(() => {
    setDidMount(true)
    
    axios.get(url+'/programOwner/getResponses',{headers:authHeader(),params:{tid:tid}})
    .then(res=>{
        
        if(res.data.status=="success" && res.data.fr.length>0)
        {
          setResponses(res.data.fr)
          var temp=[['Implementation Complexity', 'Business Value',{ role: "annotation", type: "string", p: { html: true } }]]
          for(let i=0;i<res.data.fr.length;i++)
          {
            temp.push([res.data.fr[i].ic,res.data.fr[i].bv,res.data.fr[i].pc+"("+res.data.fr[i].rank+")"])
          }
          setChartdata(temp)
          
        }
        else
        {
          setStatus(res.data.status)
        }
        
    })
    .catch(err=>{

      if(err && err.response.data=="Invalid usertype")
      {
        localStorage.removeItem("accessToken")
        localStorage.removeItem("refreshToken")
        localStorage.removeItem("userdetails")
        props.history.push('/')
      }
      else
      {
        var flag=checkError(err)
        if(flag==false)
        {
        setErrmsg(err.response.data)
        }

  }
      })
    return()=>setDidMount(false)
    
  },[])

  if(!didMount) {
    
    return null;
  }
  
  

  return (
    <div className={classes.root}>
      <CssBaseline />
      <SidebarMenu usertype="programowner" />
          
      
        <main className={classes.content}>
          <div className={classes.toolbar} />
          <Typography variant="h4" align="center" className={classes.surveyTitleColor}>
             Product Rollout Priority 
          </Typography>
          <br/>
          {errmsg?<Alert severity="error">{errmsg}</Alert>:<div/>}
          {status!="no response"?<div>
          <Paper className={classes.paper,classes.surveyTableBorder}>
          <TableContainer >
          <Table
            className={classes.table }
            aria-labelledby="tableTitle"
            size={dense ? 'small' : 'medium'}
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              classes={classes}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={responses.length}
            />

            <TableBody>
              {stableSort(responses, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const isItemSelected = isSelected(row.name);
                  const labelId = `enhanced-table-checkbox-${index}`;

                  return (
                    <TableRow
                      
                      hover
                      onClick={(event) => handleClick(event, row.name)}
                      role="checkbox"
                      aria-checked={isItemSelected}
                      tabIndex={-1}
                      key={index}
                      selected={isItemSelected}
                    >
                     
                      <TableCell className={classes.surveyTableCellBorder} component="th" id={labelId} align="center" scope="row" padding="none">
                        {row.bu}
                      </TableCell>
                      <TableCell className={classes.surveyTableCellBorder} align="center">{row.pn}</TableCell>
                      <TableCell className={classes.surveyTableCellBorder} align="center">{row.pc}</TableCell>
                      <TableCell className={classes.surveyTableCellBorder} align="center">{row.bv}</TableCell>
                      <TableCell className={classes.surveyTableCellBorder} align="center">{row.ic}</TableCell>
                      <TableCell className={classes.surveyTableCellBorder} align="center"><Typography className={classes.psColor}>{row.ps}</Typography></TableCell>
                      <TableCell className={classes.surveyTableCellBorder} align="center"><Typography className={classes.rankColor}>{row.rank}</Typography></TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: (dense ? 33 : 53) * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
                    
            </Table>
            </TableContainer>
            <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={responses.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
        </Paper>
        <br/>
        <Grid item xs={12}>
        <Chart
  width={'1065px'}
  height={'600px'}
  chartType="ScatterChart"
  loader={<div>Loading Chart</div>}
  data={chartData1}
  options={{
    title: 'Value-Complexity Matrix',
    hAxis: { title: 'Implementation Complexity' },
    vAxis: { title: 'Business Value'},
    legend: 'none',
    colors: ['#087037']
  }}
  rootProps={{ 'data-testid': '1' }}
/>
</Grid>
</div>:<Typography>No response available</Typography>}

        </main>
      
    </div>
  )
}
