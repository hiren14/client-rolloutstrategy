import React,{useState} from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { Link } from "react-router-dom";
import { url } from '../commonFile'
import axios from 'axios';
import {useStyles} from '../assets/jss/styles'


function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
        TCS 
    </Typography>
  );
}


export default function ForgetPassword() {
  const classes = useStyles();

  const [email,setEmail]=useState("");
  const [otp,setOtp]=useState("")
  const [id,setId]=useState("")
  const [status,setStatus]=useState("")
  const [flag,setFlag]=useState(true)
  const [flag1,setFlag1]=useState(true)
  const [password,setPassword]=useState("")
  const [cpassword,setCPassword]=useState("")
  const [status1,setStatus1]=useState("")
  


  const onChangeEmail=(e)=>
  {
      setEmail(e.target.value)
  }

  const onChangeOtp=(e)=>
  {
      setOtp(e.target.value)
  }

  const onChangePassword=(e)=>
  {
      setPassword(e.target.value)
  }

  const onChangeCPassword=(e)=>
  {
      setCPassword(e.target.value)
  }

  

  const onSubmitEmail=(e)=>
  {
      e.preventDefault();
      const userData=
      {
          "email":email
      }

      axios.post(url+'/user/forgetPassword',userData)
        .then(res=>
            {
                setStatus(res.data.message)
                setFlag(false)
                setId(res.data.id)
            })
        .catch(()=>alert("Please enter valid email address"))
      
  }

  const onSubmitOtp=(e)=>
  {
      e.preventDefault();
      axios.post(url+'/user/verifyOtp',{otp,email})
      .then(res=>{
        if(res.data.message=="successfull")
          setFlag1(false)
        else
           alert(res.data.message)
      })
      .catch(err=>alert(err))
  }

  const onSubmitPassword=(e)=>
  {
      e.preventDefault();
      if(password==cpassword)
       {
        const userData=
        {
            "id":id,
            "password":password
        }
  
        axios.put(url+'/user/resetPassword',userData)
          .then(res=>
              {
                  
                  setStatus1(res.data.message)
                  setPassword("")
                  setCPassword("")
            
                  
              })
          .catch(err=>alert(err))
        
       }
       
      else
      {
          alert("Please enter same passwords")
      }
  }

  const showData=()=>
  {
      return(
          <div>
          {
              flag?
              <div className={classes.paper}>
      
            
              <Typography component="h1" variant="h5">
               Enter Email
              </Typography>
              <br/>
              <form className={classes.form} noValidate>
                <TextField
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  id="email"
                  label="Email Address"
                  name="email"
                  value={email}
                  onChange={(val)=>onChangeEmail(val)}
                  autoComplete="email"
                  autoFocus
                />
                
                
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                  onClick={onSubmitEmail}
                  className={classes.submit}
                >
                  Send Otp
                </Button>
                
              </form>
            </div>:<div className={classes.paper}>
            
              
            <Typography component="h1" variant="h5">
              Enter Otp
            </Typography>
            <br/>
            <Typography  variant="body1">
              {status}
            </Typography>
            <br/>
            <form className={classes.form} noValidate>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="otp"
                label="OTP"
                name="otp"
                value={otp}
                onChange={(val)=>onChangeOtp(val)}
                autoFocus
              />
              
              
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                onClick={onSubmitOtp}
                className={classes.submit}
              >
                Verify Otp
              </Button>
              
            </form>
          </div>
        
        }
        </div>
      )
  }

  const showResetPassword=()=>
  {
      return(
        <div className={classes.paper}>
      
            
        <Typography component="h1" variant="h5">
         Reset Password
        </Typography>
        <br/>
        <Typography component="h1" variant="body1">
         { status1 }
        </Typography>
        <Link to="/">
         {
         status1!=""?"Login to continue":""
         
         }
        </Link>
        <br/>
        <form className={classes.form} noValidate>
        <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            value={password}
            onChange={(val)=>onChangePassword(val)}
            label="Password"
            type="password"
            id="password"
            
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="cpassword"
            value={cpassword}
            onChange={(val)=>onChangeCPassword(val)}
            label="Confirm Password"
            type="password"
            id="cpassword"
            
          />
          
          
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            onClick={onSubmitPassword}
            className={classes.submit}
          >
            Reset Password
          </Button>
          
        </form>
      </div>
      )
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
        <Typography variant="h3" color="primary" align="center">
                Rollout Strategy
        </Typography>
    
        {
        flag1?showData():showResetPassword()
        
        }
      
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}