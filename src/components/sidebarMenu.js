import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import HomeIcon from '@material-ui/icons/Home';
import PowerSettingsNewIcon from '@material-ui/icons/PowerSettingsNew';
import AddIcon from '@material-ui/icons/Add';
import MenuIcon from '@material-ui/icons/Menu';
import GroupIcon from '@material-ui/icons/Group';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import BusinessIcon from '@material-ui/icons/Business';
import { Link } from "react-router-dom";
import { withRouter } from 'react-router';
import axios from 'axios';
import {url,checkError} from '../commonFile'
import { makeStyles, useTheme } from '@material-ui/core/styles';
import RolloutStrategy from '../assets/images/RolloutStrategy.png';
import {useStyles} from '../assets/jss/styles'


const drawerWidth = 240;

const useStylesSidebar = makeStyles((theme) => ({
  
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appBar: {
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
    },
    backgroundColor: '#000000',
    color: '#F03782',
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  
  
  drawerPaper: {
    width: drawerWidth,
  },
  
}));


function SidebarMenu(props) {

  const { window } = props;
  const classes = useStyles();
  const classesSidebar = useStylesSidebar();
  const theme = useTheme();
  const [mobileOpen, setMobileOpen] = React.useState(false);



  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  

  const logout=()=>
  {
    let rt=localStorage.getItem("refreshToken")
    localStorage.removeItem("refreshToken")
    localStorage.removeItem("accessToken")
    localStorage.removeItem("userdetails")
    axios.delete(url+'/user/logout',{data:{refreshToken:rt}})
    .then(()=>{
      props.history.push('/')
    })
    .catch(err=>checkError(err))
  }

  const drawer = (
    <div>
      <div className={classes.toolbar} />
      <Divider />
      
      <List>
        {props.usertype=="admin"?<div>
        <Link to='/adminHome' className={classes.linkStyle}>
        <ListItem button>
            <ListItemIcon><HomeIcon/> </ListItemIcon>
            <ListItemText primary="Home" />
        </ListItem>
        </Link>
        <Link to='/addTemplate' className={classes.linkStyle}>
          <ListItem button> 
              <ListItemIcon><AddIcon/> </ListItemIcon>
              <ListItemText primary="Add Templates" />
          </ListItem>
        </Link>
        <Link to='/manageBU' className={classes.linkStyle}>
          <ListItem button> 
              <ListItemIcon><BusinessIcon/> </ListItemIcon>
              <ListItemText primary="Manage Business Units" />
          </ListItem>
        </Link>
        
        <Link to='/manageUser' className={classes.linkStyle}>
          <ListItem button> 
              <ListItemIcon><GroupIcon/> </ListItemIcon>
              <ListItemText primary="Manage Users" />
          </ListItem>
        </Link>
        </div>:<div>

        <Link to='/programOwner' className={classes.linkStyle}>
        <ListItem button>
            <ListItemIcon><HomeIcon/> </ListItemIcon>
            <ListItemText primary="Home" />
        </ListItem>
        </Link>
        <Link to='/manageProgramOwnerBU' className={classes.linkStyle}>
          <ListItem button> 
          <ListItemIcon><BusinessIcon/> </ListItemIcon>
              <ListItemText primary="Manage Business Units" />
          </ListItem>
        </Link>
        <Link to='/manageProgramOwnerUser' className={classes.linkStyle}>
          <ListItem button> 
              <ListItemIcon><GroupIcon/> </ListItemIcon>
              <ListItemText primary="Manage Business Users" />
          </ListItem>
        </Link>
        <Link to='/manageProducts' className={classes.linkStyle}>
          <ListItem button> 
              <ListItemIcon><AddIcon/> </ListItemIcon>
              <ListItemText primary="Manage Products" />
          </ListItem>
        </Link>
        </div>}
        <ListItem button onClick={()=>{logout()}} > 
            <ListItemIcon><PowerSettingsNewIcon/> </ListItemIcon>
            <ListItemText primary="Logout" />
        </ListItem>
      </List>
        
        
      
        
       

    </div>
  );

  const container = window !== undefined ? () => window().document.body : undefined;

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classesSidebar.appBar}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            className={classesSidebar.menuButton}
          >
            <MenuIcon />
          </IconButton>
          <img src={RolloutStrategy} alt="Rollout Strategy"/>
          <Typography variant="h6" className={classes.title}  noWrap>
            Rollout Strategy
          </Typography>
          <Typography variant="body1" className={classes.greeting}  noWrap>
           hello { JSON.parse(localStorage.getItem("userdetails")).username }
          </Typography>
        </Toolbar>
      </AppBar>
      <nav className={classesSidebar.drawer} aria-label="mailbox folders">
        
        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classesSidebar.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, 
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classesSidebar.drawerPaper,
            }}
            variant="permanent"
            open
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
      
    </div>
  );
}

SidebarMenu.propTypes = {
  
  window: PropTypes.func,
};

export default withRouter(SidebarMenu);
