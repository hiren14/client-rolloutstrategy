import React,{useEffect,useState} from 'react'
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Popover from '@material-ui/core/Popover';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import {authHeader ,url,checkError} from '../commonFile'
import {useStyles} from '../assets/jss/styles'
import { Link } from "react-router-dom";
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import Alert from '@material-ui/lab/Alert';
import SidebarMenu from './sidebarMenu'

export default function ProgramOwner(props) {

  const classes = useStyles();
  
  const [errmsg,setErrmsg]=useState("")
  const [templates,setTemplates]=useState([])
  const [atemplates,setATemplates]=useState([])
  const [count,setCount]=useState(0)
  const [tid,setTid]=useState("")
  
  const [status,setStatus]=useState("")
  const [didMount, setDidMount] = useState(false); 

  const [anchorEl, setAnchorEl] = React.useState(null);
  

  const onDelete=(e,id,ind)=>
  {
    e.preventDefault()
    
    axios.delete(url+'/programOwner/deleteTemplate',{headers:authHeader(),data:{tid:id}})
    .then(()=>{
      
      let narr=templates
      narr.pop(ind)
      setCount(count-1)
      setTemplates(narr)
    })
    .catch(err=>{
      var flag=checkError(err)
      if(flag==false)
      {
      setErrmsg(err.response.data)
      }

})
  }

  const selectedTemplate=(e)=>{
    setTid(e.target.value)
    
    
    props.history.push('/editProgramOwnerTemplate/'+e.target.value)

  }
  const handleClick = (event) => {

    axios.get(url+'/programOwner/getAdminTemplates',{headers:authHeader()})
    .then(res=>{

      
        if(res.data.length>0)
        {
          setATemplates(res.data)
          
          
        }
        
    })

    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;
  
  useEffect(() => {
    setDidMount(true)
    let token=localStorage.getItem("accessToken")
    
    
  
    
    if(!token)
      props.history.push('/')

    axios.get(url+'/programOwner/home',{headers:authHeader()})
    .then(res=>{
      
      
        if(res.data.length>0)
        {
          setTemplates(res.data)
          setCount(res.data.length)
          
        }
        else
        {
          setStatus("Templates are not available")
        }
    })
    .catch(err=>{

      if(err && err.response.data=="Invalid usertype")
      {
        localStorage.removeItem("accessToken")
        localStorage.removeItem("refreshToken")
        localStorage.removeItem("userdetails")
        props.history.push('/')
      }
      else
      {
        var flag=checkError(err)
        if(flag==false)
        {
        setErrmsg(err.response.data)
        }

  }
      })
    return()=>setDidMount(false)
    
  },[count])

  if(!didMount) {
    
    return null;
  }
  

  

  return (
    <div className={classes.root}>
      <CssBaseline />
      <SidebarMenu usertype="programowner" />
          
        
      
        <main className={classes.content}>
          <div className={classes.toolbar} />
          <Typography variant="h4" align="center">
              Program Owner Home Page
          </Typography>
          {errmsg?<Alert severity="error">{errmsg}</Alert>:<div/>}
          <br/>
          
          <Button
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={handleClick}
            >
            Create Template
          </Button>
          <Popover
            id={id}
            open={open}
            anchorEl={anchorEl}
            onClose={handleClose}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'center',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'center',
            }}
            
          >
            <FormControl className={classes.formControl} style={{minWidth: 140}}>
                <InputLabel id="usertype"><em>Select Template</em></InputLabel>
                <Select
                  labelId="template"
                  id="template"
                  value={tid}
                  onChange={(val)=>selectedTemplate(val,tid)}
                  
                >
                  {
                    atemplates.map((item,index)=>{
                      return <MenuItem value={item._id} key={index}>{item.templateName}</MenuItem>
                    })
                  }
                  
                  
                  
                </Select> 
          </FormControl>
          </Popover>
          <br/>
          <br/>
          <Typography variant="h5" align="left">
            Program Owner Templates -
          </Typography>
          <br/>
          <Typography variant="body1" align="left">
            { status }
          </Typography>
          <br/>
          <div>
            {
              templates.map((item,index)=>{
                return(
                  <div key={index}>
                  <Typography variant="h6">
                     {index+1} - {item.templateName}
                  </Typography>
                  <Link to={'/editProgramOwnerTemplate/'+item._id} className={classes.linkStyle}>
                    <Button
                      variant="contained"
                      color="primary"
                      className={classes.submit}
                    >
                      View and Modify Template
                    </Button>
                  </Link>
                  &nbsp;
                  &nbsp;
                  <Link to={'/showResponses/'+item._id} className={classes.linkStyle} >
                    <Button
                      variant="contained"
                      color="primary"
                      className={classes.submit}
                    >
                      View Response
                    </Button>
                  </Link>
                  &nbsp;
                  &nbsp;
                  <Link to={'/surveyCompletionReport/'+item._id} className={classes.linkStyle} >
                    <Button
                      variant="contained"
                      color="primary"
                      className={classes.submit}
                    >
                      View Survey Completion Report
                    </Button>
                  </Link>
                  &nbsp;
                  &nbsp;
                  <Button
                      variant="contained"
                      color="secondary"
                      className={classes.deleteButton}
                      onClick={(val)=>onDelete(val,item._id,index)}
                    >
                      Delete Template
                    </Button>
                  </div>

                )
                
              })
            }
          </div>
          
        </main>
      
    </div>
  )
}
