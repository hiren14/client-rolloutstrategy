import React,{useEffect,useState} from 'react'
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import {authHeader ,url,checkError} from '../commonFile'
import {useStyles} from '../assets/jss/styles'
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormGroup from '@material-ui/core/FormGroup';
import Grid from '@material-ui/core/Grid';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import FormHelperText from '@material-ui/core/FormHelperText';
import Alert from '@material-ui/lab/Alert';
import SidebarMenu from './sidebarMenu'

export default function EditProgramOwnerTemplate(props) {

  const classes = useStyles();
  
  const [questions,setQuestions]=useState([[0,[0]]])
  const [count,setCount]=useState([[0,0]])
  const [id,setId]=useState("")
  const [tid]=useState(props.match.params.id)
  const [que,setQue]=useState([["","",[{"desc":"","level":""}]]])
  const [errmsg,setErrmsg]=useState("")
  const [template,setTemplate]=useState("")
  const [flag,setFlag]=useState(true)
  const [bus,setBUS]=useState([])
  const [check,setCheck]=useState(false)
  const [usertype, setUsertype] = React.useState({
    
  });

  useEffect(() => {
      
    
    axios.get(url+'/programOwner/getTemplate',{headers:authHeader(),params:{tid:tid}})
    .then(res=>{
        
        if(res.data.flag==true)
        {
          setFlag(res.data.flag)
          setTemplate(res.data.template.templateName)
        }
        else
        {
          setFlag(res.data.flag)
        }
        setBUS(res.data.bus)
        var temp={}
        for(let i=0;i<res.data.bus.length;i++)
        {
          temp[res.data.bus[i].businessUnit]=false
        }
        if(res.data.flag==true)
        {
          for(let j=0;j<res.data.template.visibility.length;j++)
          {
            temp[res.data.template.visibility[j]]=true
          }
        }
        
        setUsertype(temp)
        setCheck(true)
        setId(res.data.template._id)
        let narr=[]
        let narr1=[]
        let narr2=[]
        
        for(let i=0;i<res.data.template.questions.length;i++)
        {
            narr.push([res.data.template.questions[i][0],res.data.template.questions[i][1],res.data.template.questions[i][2]])
            narr1.push([i,res.data.template.questions[i][2].length])
            narr2.push([i,[0]])
            for(let j=1;j<res.data.template.questions[i][2].length;j++)
            {
                narr2[i][1].push(j)
            }

            
        }
        
        setCount(narr1)
        setQue(narr)
        setQuestions(narr2)
        
        
        
    
    })
    .catch(err=>{

      if(err && err.response.data=="Invalid usertype")
      {
        localStorage.removeItem("accessToken")
        localStorage.removeItem("refreshToken")
        localStorage.removeItem("userdetails")
        props.history.push('/')
      }
      else
      {
        var flag=checkError(err)
        if(flag==false)
        {
        setErrmsg(err.response.data)
        }

  }
      })
      
  }, [])

  
  const questionList=questions.map((item1,index)=>{

    return <div key={index}>
      <Grid container >
        <Grid item xs={12}>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id={"question"+(index+1)}
              label={"Question"+(index+1)}
              name={"question"+(index+1)}
              value={que[index][0]}
              onChange={(val)=>setQuestion(val,index)}
              
              
              autoFocus
            />
            <Grid item xs={12}>
          <FormControl className={classes.formControl}>
                <InputLabel id="usertype">Category</InputLabel>
                <Select
                  labelId="category"
                  id="category"
                  value={que[index][1]}
                  onChange={(val)=>setCategory(val,index)}
                >
                  <MenuItem value="ic">Implementation Complexity</MenuItem>
                  <MenuItem value="bv">Business Value</MenuItem>
                  
                </Select> 
          </FormControl>
          </Grid>
        </Grid>
      </Grid>
      {item1[1].map((sitem,sindex)=>{
    return <Grid container key={sindex} spacing={1}><Grid item xs={12} sm={6}><TextField
    
    variant="outlined"
    margin="normal"
    required
    fullWidth
    id={"option"+(sindex+1)}
    label={"option"+(sindex+1)}
    name={"option"+(sindex+1)}
    value={que[index][2][sindex].desc}
    onChange={(val)=>{onChangeOpvalue(val,index,sindex)}}
    autoFocus
  />
  </Grid>
  
  <Grid item xs={12} sm={6}>
  <TextField
    
    variant="outlined"
    margin="normal"
    required
    fullWidth
    id={"optionL"+(sindex+1)}
    label={"Weightage"}
    name={"optionL"+(sindex+1)}
    value={que[index][2][sindex].level}
    onChange={(val)=>{onChangeOpLevel(val,index,sindex)}}
    autoFocus
  />
  </Grid>
  </Grid>
  

})
}



<Grid container>
    <Grid item xs={12}>
      <Button
          
        onClick={(e)=>addOption(e,index)}
        variant="contained"
        color="secondary"
        className={classes.submit}
      >
        Add Option
      </Button>

      &nbsp;
      <Button
          
        onClick={(e)=>deleteOption(e,index)}
        variant="contained"
        color="secondary"
        disabled={count[index][1]==1?true:false}
        className={classes.deleteButton}
      >
        Delete Option
      </Button>
    </Grid>
  <br/>
  
  
  
</Grid>
</div>


})

const setQuestion=(e,ind)=>
{
    let arr=[...que]
    arr[ind][0]=e.target.value
    setQue(arr)

}

const setCategory=(e,ind)=>
{
    let arr=[...que]
    arr[ind][1]=e.target.value
    setQue(arr)

}



const onChangeOpvalue=(e,ind,sind)=>
{
  let narr=[...que]
  narr[ind][2][sind].desc=e.target.value
  setQue(narr)
}
const onChangeOpLevel=(e,ind,sind)=>
{
  let narr=[...que]
  narr[ind][2][sind].level=e.target.value
  setQue(narr)
}

const onChangeUsertype=(e)=>
{
  setUsertype({ ...usertype, [e.target.name]: e.target.checked });
}

const addOption=(e,ind)=>
{
  e.preventDefault()
  let temp=count
  temp[ind][1]+=1
  let temp1=[...questions]
  let temp2=[...que]
  temp1[ind][1].push(temp[ind][1])
  temp2[ind][2].push({"desc":"","level":""})
  setQuestions(temp1)
  setQue(temp2)
  setCount(temp)
}

const deleteOption=(e,ind)=>
{
 
  console.log(count)
  e.preventDefault()
  let temp=count
  temp[ind][1]-=1
  let temp1=[...questions]
  let temp2=[...que]
  temp1[ind][1].pop()
  temp2[ind][2].pop()
  setQuestions(temp1)
  setQue(temp2)
  setCount(temp)
}

const addQuestion=(e)=>
{
  e.preventDefault()
  let temp=[...count]
  temp.push([temp[temp.length-1][0]+1,0])
  let temp1=[...questions]
  let temp2=que
  temp1.push([temp[temp.length-1][0]+1,[0]])
  temp2.push(["","",[{"desc":"","level":""}]])
  setQuestions(temp1)
  setQue(temp2)
  setCount(temp)
  
}

const deleteQuestion=(e)=>
{
  e.preventDefault()
  let temp=[...count]
  let temp1=[...questions]
  let temp2=que
  temp.pop()
  temp1.pop()
  temp2.pop()
  setQuestions(temp1)
  setQue(temp2)
  setCount(temp)
}

const onSubmit=(e)=>
{
  e.preventDefault()

  
  let temp=[]
  for(let key in usertype)
  {
    if(usertype[key])
    {
      temp.push(key)
    }
  }
  
  const data={
    "templateName":template,
    "questions":que,
    "visibility":temp,
    "tid":tid,
    "id":id,
    "flag":flag
    
  }
  
  axios.post(url+'/programOwner/saveTemplate',data,{headers:authHeader()})
  .then(()=>{

      
      props.history.push('/programOwner')

  })
  .catch(err=>{
            var flag=checkError(err)
            if(flag==false)
            {
            setErrmsg(err.response.data)
            
          }
  })
}

  

  return (
    <div className={classes.root}>
      <CssBaseline />
      <SidebarMenu usertype="programowner" />
      
        <main className={classes.content}>
          <div className={classes.toolbar} />
          {errmsg?<Alert severity="error">{errmsg}</Alert>:<div/>}

        <form className={classes.form} noValidate >
        <Grid container >
            <Grid item xs={12}>
                    <TextField
                      variant="outlined"
                      margin="normal"
                      required
                      fullWidth
                      id="Template"
                      label="Template"
                      name="template"
                      value={template}
                      onChange={(e)=>setTemplate(e.target.value)}
                      autoComplete="template"
                      autoFocus
                    />
            </Grid>
            </Grid>
            
            {questionList}
            <Grid container>
            <Button
              type="submit"
              onClick={(e)=>addQuestion(e)}
              variant="contained"
              color="secondary"
              
              className={classes.submit}
            >
              Add Question
            </Button>
            &nbsp;
            <Button
              type="submit"
              onClick={(e)=>deleteQuestion(e)}
              variant="contained"
              color="secondary"
              disabled={count.length==1?true:false}
              className={classes.deleteButton}
            >
              Delete Question
            </Button>
            </Grid>
            <br/>
            <FormHelperText style={{fontSize:20,color:'black'}}>Visible To</FormHelperText>
            <Grid item xs={12}>
            <FormGroup row>
              {
                bus.map((item1,index)=>{
                  return <FormControlLabel
                  key={index}
                  control={
                  <Checkbox 
                  checked={check && usertype[item1.businessUnit]} 
                  color="primary" 
                  onChange={onChangeUsertype} 
                  name={item1.businessUnit} 
                  />
                }
                  label={item1.businessUnit}
                />
                })
              }
              
              
            </FormGroup>
            </Grid>
            <Button
              type="submit"
              onClick={(e)=>onSubmit(e)}
              variant="contained"
              color="primary"
              disabled={template==""?true:false}
              className={classes.submit}
            >
              Save Template
            </Button>
        
        
      </form>
        </main>
      
    </div>
  )
}
