import React,{useEffect,useState} from 'react'
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Alert from '@material-ui/lab/Alert';
import axios from 'axios';
import {authHeader ,url,checkError} from '../commonFile'
import {useStyles} from '../assets/jss/styles'

import SidebarMenu from './sidebarMenu'
import MaterialTable from "material-table";

import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import { forwardRef } from 'react';


export default function ManageProducts(props) {

  const classes = useStyles();
  const [errmsg,setErrmsg]=useState("")
  const [count,setCount]=useState(0)
  const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
  };
  
  const [didMount, setDidMount] = useState(false); 

  const [bus,setBus]=useState({})


  const [data, setData] = useState([
    
  ]);
  
  useEffect(() => {
    setDidMount(true)
    
    axios.get(url+'/programOwner/getProducts',{headers:authHeader(),params:{count:count}})
    .then(res=>{
       
        if(count==0)
        {
            var temp={}
            for(let i=0;i<res.data.bus.length;i++)
            {
                temp[res.data.bus[i].businessUnit]=res.data.bus[i].businessUnit
            }
            setBus(temp)
        }
        setData(res.data.products)
        setCount(res.data.products.length)
    })
    .catch(err=>{

        if(err && err.response.data=="Invalid usertype")
        {
          localStorage.removeItem("accessToken")
          localStorage.removeItem("refreshToken")
          localStorage.removeItem("userdetails")
          props.history.push('/')
        }
        else
        {
          var flag=checkError(err)
          if(flag==false)
          {
          setErrmsg(err.response.data)
          }
  
    }
})

    return()=>setDidMount(false)
    
  },[count])

  if(!didMount) {
    
    return null;
  }
  


  return (
    <div className={classes.root}>
      <CssBaseline />
      <SidebarMenu usertype="programowner" />
          
      
        <main className={classes.content}>
          <div className={classes.toolbar} />
          <Typography variant="h4" align="center">
              Manage Products
          </Typography>
          <br/>
          {errmsg?<Alert severity="error">{errmsg}</Alert>:<div/>}


          <MaterialTable
    	    icons={tableIcons}
            title="Products"
            data={data}
            options={{ search: false }}
            columns={[
                { title: 'Product Name', field: 'productName' },
                { title: 'Product Code', field: 'productCode' },
                { title: 'Business Unit', field: 'businessUnit', lookup: bus },
    
            ]}
            editable={{
                onRowAdd: newData =>
                new Promise((resolve, reject) => {
                    
                    
                    const d={'productName':newData.productName,'productCode':newData.productCode,'businessUnit':newData.businessUnit}
                    axios.post(url+'/programOwner/addProduct',d,{headers:authHeader()})
                    .then(()=>{
                        setCount(count+1)
                        setErrmsg("")
                        resolve();
                    })
                    .catch(err=>{
                            var flag=checkError(err)
                            if(flag==false)
                            {
                            setErrmsg(err.response.data)
                            }
                        reject()
                    })
                    
                   
                }),
                onRowUpdate: (newData, oldData) =>
                new Promise((resolve, reject) => {
                    
                    const d={'pid':newData._id,'productName':newData.productName,'productCode':newData.productCode,'businessUnit':newData.businessUnit}
                    axios.put(url+'/programOwner/editProduct',d,{headers:authHeader()})
                    .then(()=>{
                        const dataUpdate = [...data];
                        const index = oldData.tableData.id;
                        dataUpdate[index] = newData;
                        setData([...dataUpdate]);
                        setErrmsg("")
                        resolve();
                    })
                    .catch(err=>{
                            var flag=checkError(err)
                            if(flag==false)
                            {
                            setErrmsg(err.response.data)
                            }
                        reject()
                    })
                    
                    
                }),
                onRowDelete: oldData =>
                new Promise((resolve, reject) => {
                    
                    
                    axios.delete(url+'/programOwner/deleteProduct',{headers:authHeader(),data:{pid:oldData._id}})
                    .then(()=>{
                    
                    setCount(count-1)
                    setErrmsg("")
                    resolve()
                    })
                    .catch(err=>{
                            var flag=checkError(err)
                            if(flag==false)
                            {
                            setErrmsg(err.response.data)
                            }
                        reject()
                    })
                    
                    
                }),
            }}
    />
    
                  
        </main>
      
    </div>
  )
}
