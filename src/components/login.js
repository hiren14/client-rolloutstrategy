import React,{useState} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { Link } from "react-router-dom";
import { url } from '../commonFile'
import axios from 'axios';
import {useStyles} from '../assets/jss/styles'
import Alert from '@material-ui/lab/Alert';


function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
        TCS
    </Typography>
  );
}


export default function Login(props) {
  const classes = useStyles();

  const [username,setUsername]=useState("");
  const [password,setPassword]=useState("");
  const [errmsg,setErrmsg]=useState("")


  const onChangeUsername=(e)=>
  {
      setUsername(e.target.value)
  }

  const onChangePassword=(e)=>
  {
      setPassword(e.target.value)
  }

  const onSubmit=(e)=>
  {
      e.preventDefault();
      const userData=
      {
          "username":username,
          "password":password
      }

      axios.post(url+'/user/login',userData)
        .then(res=>
            {
                
                
                if(res.data.accessToken && res.data.refreshToken)
                {
                    
                    localStorage.setItem("accessToken",res.data.accessToken)
                    localStorage.setItem("refreshToken",res.data.refreshToken)
                    localStorage.setItem("userdetails",JSON.stringify(res.data.user))
                    let usertype=res.data.user.usertype
                    if(usertype=="admin")
                        props.history.push('/adminHome')
                    else if(usertype=="programowner")
                        props.history.push('/programOwner')
                    else
                        props.history.push('/userHome')
                }   
                
            })
        .catch(()=>
          {
            
          setErrmsg("Wrong Credentials")
          })
      
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
        <Typography variant="h3" color="primary" align="center">
                Rollout Strategy
        </Typography>
      <div className={classes.paper}>
      
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        {errmsg?<Alert severity="error">{errmsg}</Alert>:<div/>}
        <form className={classes.form} noValidate onSubmit={(e)=>onSubmit(e)}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="username"
            label="User Name"
            name="username"
            value={username}
            onChange={(val)=>onChangeUsername(val)}
            autoFocus
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            value={password}
            onChange={(val)=>onChangePassword(val)}
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
          />
        
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign In
          </Button>
          <Grid container>
            <Grid item xs>
              <Link to='/forgetPassword'>
                Forgot password?
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}