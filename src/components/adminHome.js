import React,{useEffect,useState} from 'react'
import SidebarMenu from './sidebarMenu'
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import axios from 'axios';
import {authHeader ,url,checkError} from '../commonFile'
import {useStyles} from '../assets/jss/styles'
import Alert from '@material-ui/lab/Alert';
import { Link } from "react-router-dom";


export default function AdminHome(props) {

  const classes = useStyles();
  
  const [errmsg,setErrmsg]=useState("")
  const [templates,setTemplates]=useState([])
  const [count,setCount]=useState(0)
  const [status,setStatus]=useState("")
  const [didMount, setDidMount] = useState(false); 
  
  useEffect(() => {
    setDidMount(true)
    let token=localStorage.getItem("accessToken")
    
    
  
    
    if(!token)
      props.history.push('/')

    axios.get(url+'/admin/home',{headers:authHeader()})
    .then(res=>{

      
        if(res.data.length>0)
        {
          setTemplates(res.data)
          setCount(res.data.length)
          
        }
        else
        {
          setStatus("Templates are not available.Please Add some")
        }
    })
    .catch(err=>{

      if(err && err.response.data=="Invalid usertype")
      {
        localStorage.removeItem("accessToken")
        localStorage.removeItem("refreshToken")
        localStorage.removeItem("userdetails")
        props.history.push('/')
      }
      else{
            var flag=checkError(err)
            if(flag==false)
            {
            setErrmsg(err.response.data)
            }

      }
        
      })
    return()=>setDidMount(false)
    
  },[count])

  if(!didMount) {
    
    return null;
  }
  


  return (
    <div className={classes.root}>
      <CssBaseline/>
      <SidebarMenu usertype="admin" />
      
        <main className={classes.content}>
          <div className={classes.toolbar} />
          <Typography variant="h4" align="center">
              Admin Home Page
          </Typography>
          <br/>
          <Typography variant="body1" align="left">
            { status }
          </Typography>
          
          {errmsg?<Alert severity="error">{errmsg}</Alert>:<div/>}
          <br/>
          
          <div>
            {
              templates.map((item,index)=>{
                return(
                  <div key={index}>
                  <Typography variant="h6">
                     {item.templateName}
                  </Typography>
                  <Link to={'/showTemplate/'+item._id} className={classes.linkStyle}>
                    <Button
                      variant="contained"
                      color="primary"
                      className={classes.submit}
                    >
                      View Template
                    </Button>
                  </Link>
                  </div>

                )
                
              })
            }
          </div>

        </main>
      
    </div>
  )
}